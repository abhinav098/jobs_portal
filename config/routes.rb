Rails.application.routes.draw do

  devise_for :users
  resources :jobs
  resources :charges
  root 'jobs#index'
end
