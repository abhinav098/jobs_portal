class Job < ActiveRecord::Base
  belongs_to :category
  belongs_to :user

  validates :category , presence: :true
  validates :title, presence: :true
  validates :description, presence: :true
  validates :company, presence: :true
  validates :url, presence: :true

end
